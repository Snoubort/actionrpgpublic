// Made by DemoDreams Team.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RPGCharacter.generated.h"

UCLASS()
class ACTIONRPG_API ARPGCharacter : public ACharacter
{
	GENERATED_BODY()
	
public:
	ARPGCharacter();
	
protected:
	virtual void BeginPlay() override;
	
public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
};